defmodule ShatServer do
  use Application

  alias ShatServer.UserAgent

  def accept_opts, do: [:binary, packet: :line, active: false, reuseaddr: true]
  def port,        do: Application.get_env(:shat_server, :port)
  def version,     do: ShatServer.Mixfile.project[:version]

  def start(_type, _args) do
    IO.puts "Shat server #{version}"

    ShatServer.Supervisor.start_link()
  end

  def accept(port \\ 80) do
    {:ok, conn} = :gen_tcp.listen(port, accept_opts)

    IO.puts "Listening on port #{port}..."

    loop_acceptor(conn)
  end

  defp loop_acceptor(conn) do
    {:ok, client} = :gen_tcp.accept(conn)

    UserAgent.register(client, which_addr(client))

    put_info("Connection from #{which_addr(client)}")

    {:ok, pid} = Task.Supervisor.start_child(ShatServer.TaskSupervisor, fn -> handle(client, :init) end)
    :ok = :gen_tcp.controlling_process(client, pid)

    loop_acceptor(conn)
  end

  defp read_line(client) do
    case :gen_tcp.recv(client, 0) do
      {:ok, raw_data} ->
        IO.puts("<====== #{raw_data}")
        case Poison.decode(raw_data) do
          {:ok, json} ->
            json
          {:error, _} ->
            raw_data
        end
      {:error, :closed} ->
        put_info("Client left")
        Process.exit(Kernel.self, :kill)
    end
  end

  defp handle(client) do
    client |> handle(read_line(client))
  end

  defp handle(client, :init) do
    write_line(client, "Welcome to Shat Server #{version}")
    handle(client)
  end

  defp handle(client, %{"connection" => %{"login" => login}}) do
    case UserAgent.get(client) do
      {:ok, user} ->
        user = UserAgent.update_and_get!(client, :login, login)
        put_success("[+] #{inspect(user)}")
        write_line(client, "ok")
      {:error, _} ->
        put_error()
        write_line(client, "Dupplicate username, please take another one")
    end

    handle(client)
  end

  defp handle(client, _unknown_message) do
    write_line(client, "Sorry ?")
    handle(client)
  end

  defp which_addr(client) do
    {:ok, {addr, _}} = :inet.peername(client)
    addr |> :erlang.tuple_to_list() |> Enum.join(".")
  end

  defp write_line(client, line) do
    IO.puts "======> #{line}"

    :gen_tcp.send(client, line <> "\n")
  end

  defp put_error(msg \\ "Error") do
    IO.puts(IO.ANSI.red <> msg <> IO.ANSI.reset)
  end

  defp put_info(msg) do
    IO.puts(IO.ANSI.yellow <> msg <> IO.ANSI.reset)
  end

  defp put_success(msg \\ "Success") do
    IO.puts(IO.ANSI.green <> msg <> IO.ANSI.reset)
  end
end
