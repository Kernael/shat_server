defmodule ShatServer.Supervisor do
  use Supervisor

  def init(:ok) do
    children = [supervisor(Task.Supervisor, [[name: ShatServer.TaskSupervisor]]),
                worker(Task, [ShatServer, :accept, [ShatServer.port]]),
                worker(ShatServer.UserAgent, [])]

    supervise(children, strategy: :one_for_one)
  end

  def start_link do
    Supervisor.start_link(__MODULE__, :ok, name: __MODULE__)
  end
end
