defmodule ShatServer.UserAgent do
  alias ShatServer.User

  def start_link do
    Agent.start_link(fn -> Map.new end, name: __MODULE__)
  end

  def exists?(client) do
    Agent.get(__MODULE__, &Map.has_key?(&1, client))
  end

  def get(client) do
    Agent.get(__MODULE__, &Map.fetch(&1, client))
  end

  def get!(client) do
    Agent.get(__MODULE__, &Map.fetch!(&1, client))
  end

  def register(client, addr \\ "") do
    Agent.update(__MODULE__, &Map.put(&1, client, %User{addr: addr}))
    get(client)
  end

  def list do
    Agent.get(__MODULE__, &Map.keys(&1))
  end

  def update!(client, key, login) do
    Agent.update(__MODULE__, fn(map) ->
      Map.update!(map, client, fn(user) ->
        Map.update!(user, key, fn(_val) ->
          login
        end)
      end)
    end)
  end

  def update_and_get!(client, key, login) do
    update!(client, key, login)
    get!(client)
  end
end
